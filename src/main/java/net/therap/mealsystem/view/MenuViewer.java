package net.therap.mealsystem.view;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.domain.Menu;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class MenuViewer implements Viewer<Menu> {

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;

    public MenuViewer(FoodDao foodDao, MealTimeDao mealTimeDao) {
        this.foodDao = foodDao;
        this.mealTimeDao = mealTimeDao;
    }

    @Override
    public void view(Menu item) throws SQLException {
        ArrayList<Menu> items = new ArrayList<>();
        items.add(item);
        view(items);
    }

    @Override
    public void view(List<Menu> menus) throws SQLException {
        String[] days = new String[] {"saturday", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday"};

        view(menus, days);
    }

    public void view(List<Menu> menus, String[] days) {
        Map<String, List<Menu>> routines = new HashMap<>();
        for (String day : days) {
            routines.put(day, new ArrayList<>());
        }

        for (Menu menu : menus) {
            routines.get(menu.getMealTime().getDay()).add(menu);
        }

        for (String day : days) {
            System.out.println(day);
            System.out.println("---------------------");

            for (Menu menu : routines.get(day)) {
                System.out.print(menu.getMealTime().getTime());
                System.out.print(": ");
                for (int i = 0; i < menu.getMenuItems().size(); i++) {
                    if (i > 0) {
                        System.out.print(", ");
                    }
                    String foodName = menu.getMenuItems().get(i).getFood().getName();
                    String maxAmount = menu.getMenuItems().get(i).getMaximumAmount();
                    System.out.printf("%s (%s)", foodName, maxAmount);
                }
                System.out.println();
            }

            System.out.println();
            System.out.println();
        }
    }
}
