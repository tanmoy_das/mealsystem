# Meal System for Therap (Assignment 3)

This is the project containing a simple meal management system for Therap BD as a part of Assignment 3 of Therap Training System.

Remember to migrate the first time you run this.

**Commands:**

* _Migrate:_ gradle build && gradle -q run --args='--migrate'
 
* _Rollback Migrations:_ gradle -q run --args='--migrate --rollback'

* _Add Food:_ gradle build && gradle -q run --args='--addFood "vegetable curry"'

* _Delete Food:_ gradle build && gradle -q run --args='--deleteFood "vegetable curry"'

* _Show Foods:_ gradle build && gradle -q run --args='--showFoods'

* _Show Foods (alternative):_ gradle build && gradle -q run --args='--viewFoods'

* _Add Item to Menu:_ gradle build && gradle -q run --args='--addItem "vegetable curry" --day saturday --time lunch --maxAmount 1pc'

* _Remove Item from Menu:_ gradle build && gradle -q run --args='--removeItem "vegetable curry" --day saturday --time lunch --maxAmount 1pc'

* _Show entire Menu:_ gradle build && gradle -q run --args='--viewMenu'

* _Show menu by day:_ gradle build && gradle -q run --args='--viewMenu --day saturday'

* _Show entire Menu (alternative):_ gradle build && gradle -q run --args='--showMenu'

* _Show menu by day (alternative):_ gradle build && gradle -q run --args='--showMenu --day saturday'
