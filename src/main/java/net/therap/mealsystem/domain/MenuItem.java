package net.therap.mealsystem.domain;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class MenuItem {

    private int id;
    private Food food;
    private String maximumAmount;

    public MenuItem() {
    }

    public MenuItem(Food food, String maximumAmount) {
        this.food = food;
        this.maximumAmount = maximumAmount;
    }

    public MenuItem(int id, Food food, String maximumAmount) {
        this.id = id;
        this.food = food;
        this.maximumAmount = maximumAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }
}
