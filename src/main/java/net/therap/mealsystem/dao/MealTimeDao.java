package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.mapper.MealTimeMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class MealTimeDao {

    private Connection connection;
    private MealTimeMapper mealTimeMapper;

    public MealTimeDao(Connection connection, MealTimeMapper mealTimeMapper) throws SQLException {
        this.connection = connection;
        this.mealTimeMapper = mealTimeMapper;
    }

    public Optional<MealTime> get(int id) throws SQLException {
        String sqlSelect = "SELECT * FROM mealTimes where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        return mealTimeMapper.map(resultSet);
    }

    public List<MealTime> getAll() throws SQLException {
        List<MealTime> mealTimes = new ArrayList<>();
        String sqlSelect = "SELECT * FROM mealTimes";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (!resultSet.isAfterLast()) {
            Optional<MealTime> optionalMealTime = mealTimeMapper.map(resultSet);
            optionalMealTime.ifPresent(mealTimes:: add);
        }

        return mealTimes;
    }

    public Optional<MealTime> save(MealTime mealTime) throws SQLException {
        String sqlInsert = "INSERT INTO mealTimes (day, time) VALUES (?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
        preparedStatement.setString(1, mealTime.getDay());
        preparedStatement.setString(2, mealTime.getTime());
        preparedStatement.executeUpdate();

        return getByDayTime(mealTime.getDay(), mealTime.getTime());
    }

    public Optional<MealTime> getByDayTime(String day, String time) throws SQLException {
        String sqlSelect = "SELECT * FROM mealTimes where day = ? and time = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        preparedStatement.setString(1, day);
        preparedStatement.setString(2, time);


        ResultSet resultSet = preparedStatement.executeQuery();
        return mealTimeMapper.map(resultSet);
    }


    public void update(MealTime mealTime) throws SQLException {
        String sqlUpdate = "UPDATE mealTimes SET day = ?, time = ? WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
        preparedStatement.setString(1, mealTime.getDay());
        preparedStatement.setString(2, mealTime.getTime());
        preparedStatement.setInt(3, mealTime.getId());
        preparedStatement.executeUpdate();
    }

    public void updateAll(List<MealTime> mealTimes) throws SQLException {
        connection.setAutoCommit(false);

        for (MealTime mealTime : mealTimes) {
            update(mealTime);
        }

        connection.commit();
        connection.setAutoCommit(true);
    }

    public void delete(MealTime mealTime) throws SQLException {
        String sqlDelete = "DELETE FROM food WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
        preparedStatement.setInt(1, mealTime.getId());
        preparedStatement.executeUpdate();
    }
}
