package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.mapper.FoodMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class FoodDao {

    private Connection connection;
    private FoodMapper foodMapper;

    public FoodDao(Connection connection, FoodMapper foodMapper) throws SQLException {
        this.connection = connection;
        this.foodMapper = foodMapper;
    }

    public Optional<Food> get(int id) throws SQLException {
        String SQL_SELECT = "SELECT * FROM foods where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setLong(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        return foodMapper.map(resultSet);
    }

    public List<Food> getAll() throws SQLException {
        List<Food> foods = new ArrayList<>();
        String SQL_SELECT = "SELECT * FROM foods";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (!resultSet.isAfterLast()) {
            Optional<Food> optionalFood = foodMapper.map(resultSet);
            optionalFood.ifPresent(foods:: add);
        }

        return foods;
    }

    public Optional<Food> save(Food food) throws SQLException {
        String SQL_INSERT = "INSERT INTO foods (name) VALUES (?)";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
        preparedStatement.setString(1, food.getName());
        preparedStatement.executeUpdate();

        return getByName(food.getName());
    }

    public Optional<Food> getByName(String name) throws SQLException {
        String SQL_SELECT = "SELECT * FROM foods where name=?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setString(1, name);

        ResultSet resultSet = preparedStatement.executeQuery();
        return foodMapper.map(resultSet);
    }

    public void update(Food food) throws SQLException {
        String SQL_UPDATE = "UPDATE foods SET name=?, id=? WHERE id=?";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);
        preparedStatement.setString(1, food.getName());
        preparedStatement.setInt(2, food.getId());
        preparedStatement.setInt(3, food.getId());
        preparedStatement.executeUpdate();
    }

    public void updateAll(List<Food> foods) throws SQLException {
        connection.setAutoCommit(false);

        for (Food food : foods) {
            update(food);
        }

        connection.commit();
        connection.setAutoCommit(true);
    }

    public void delete(Food food) throws SQLException {
        String SQL_DELETE = "DELETE FROM foods WHERE id=?";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE);
        preparedStatement.setInt(1, food.getId());
        preparedStatement.executeUpdate();
    }
}
