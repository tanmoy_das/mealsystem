package net.therap.mealsystem.domain;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class MealTime {

    private int id;
    private String day;
    private String time;

    public MealTime() {
    }

    public MealTime(String day, String time) {
        this.day = day;
        this.time = time;
    }

    public MealTime(int id, String day, String time) {
        this.id = id;
        this.day = day;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
