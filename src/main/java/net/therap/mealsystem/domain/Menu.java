package net.therap.mealsystem.domain;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class Menu {

    private MealTime mealTime;
    private List<MenuItem> menuItems;

    public Menu() {
    }

    public Menu(MealTime mealTime, List<MenuItem> menuItems) {
        this.mealTime = mealTime;
        this.menuItems = menuItems;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public void setMealTime(MealTime mealTime) {
        this.mealTime = mealTime;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
