package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.mapper.MenuMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class MenuDao {

    private Connection connection;
    private MenuMapper menuMapper;
    private MealTimeDao mealTimeDao;
    private MenuItemDao menuItemDao;

    public MenuDao(Connection connection, MenuMapper menuMapper, MealTimeDao mealTimeDao, MenuItemDao menuItemDao) {
        this.connection = connection;
        this.menuMapper = menuMapper;
        this.mealTimeDao = mealTimeDao;
        this.menuItemDao = menuItemDao;
    }

    public Optional<Menu> get(MealTime mealTime) throws SQLException {
        String sqlSelect = "SELECT * FROM menuItems where mealTimeId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
        preparedStatement.setInt(1, mealTime.getId());

        ResultSet resultSet = preparedStatement.executeQuery();

        return menuMapper.map(resultSet);
    }

    public List<Menu> getAll() throws SQLException {
        List<Menu> menus = new ArrayList<>();

        List<MealTime> mealTimes = mealTimeDao.getAll();
        for (MealTime mealTime : mealTimes) {
            Optional<Menu> optionalMenu = get(mealTime);
            optionalMenu.ifPresent(menus:: add);
        }

        return menus;
    }

    public List<Menu> getByDay(String day) throws SQLException {
        List<Menu> allMenus = getAll();

        ArrayList<Menu> menus = new ArrayList<>();
        for (Menu menu : allMenus) {
            if (menu.getMealTime().getDay().equals(day)) {
                menus.add(menu);
            }
        }

        return menus;
    }

    public void save(Menu menu) throws SQLException {
        for (MenuItem menuItem : menu.getMenuItems()) {
            menuItemDao.save(menu.getMealTime(), menuItem);
        }
    }

    public void update(Menu menu) throws SQLException {
        for (MenuItem menuItem : menu.getMenuItems()) {
            menuItemDao.update(menu.getMealTime(), menuItem);
        }
    }

    public void delete(MenuItem menuItem) throws SQLException {
        menuItemDao.delete(menuItem);
    }
}
