package net.therap.mealsystem.migration;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class MealTimeMigration implements Migratable {

    @Override
    public void migrate(Connection connection) throws SQLException {
        String sqlCreate = "CREATE TABLE IF NOT EXISTS mealTimes"
                + "("
                + " id INT NOT NULL AUTO_INCREMENT,"
                + " day varchar(100) NOT NULL,"
                + " time varchar(100) NOT NULL,"
                + " PRIMARY KEY (id)"
                + ")";

        connection.prepareStatement(sqlCreate).execute();
    }

    @Override
    public void rollback(Connection connection) throws SQLException {
        String sqlDrop = "DROP TABLE mealTimes";
        connection.prepareStatement(sqlDrop).execute();
    }
}
