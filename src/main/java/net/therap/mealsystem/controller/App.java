package net.therap.mealsystem.controller;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.MenuItemDao;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.helper.MigrationHelper;
import net.therap.mealsystem.mapper.FoodMapper;
import net.therap.mealsystem.mapper.MealTimeMapper;
import net.therap.mealsystem.mapper.MenuItemMapper;
import net.therap.mealsystem.mapper.MenuMapper;
import net.therap.mealsystem.util.CliHandler;
import net.therap.mealsystem.view.FoodViewer;
import net.therap.mealsystem.view.MenuViewer;
import net.therap.mealsystem.view.Viewer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class App {

    private Connection connection;

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;
    private MenuItemDao menuItemDao;
    private MenuDao menuDao;


    public App(Connection connection) throws SQLException {
        this.connection = connection;
        this.foodDao = new FoodDao(connection, new FoodMapper());
        this.mealTimeDao = new MealTimeDao(connection, new MealTimeMapper());
        this.menuItemDao = new MenuItemDao(connection, new MenuItemMapper(foodDao));
        this.menuDao = new MenuDao(connection, new MenuMapper(mealTimeDao, menuItemDao), mealTimeDao, menuItemDao);
    }

    public void run(String[] args) throws SQLException {
        if (connection != null) {
            try {
                CliHandler cliHandler = new CliHandler();

                cliHandler.addOption("addFood", true, "Add Food to Food List");
                cliHandler.addOption("deleteFood", true, "Delete Food from Food List");
                cliHandler.addOption("showFoods", false, "Show All Foods");
                cliHandler.addOption("viewFoods", false, "Show All Foods");

                cliHandler.addOption("day", true, "Day");
                cliHandler.addOption("time", true, "Time");
                cliHandler.addOption("addItem", false, "Add Item to meal");
                cliHandler.addOption("maxAmount", true, "Maximum amount per person");
                cliHandler.addOption("removeItem", false, "Remove Item from meal");
                cliHandler.addOption("updateItem", false, "Update Item from meal");

                cliHandler.addOption("viewMenu", false, "View Menu");
                cliHandler.addOption("showMenu", false, "View Menu");

                cliHandler.addOption("migrate", "migrate",
                        false, "Migrate the Tables");
                cliHandler.addOption("rollback", "rollback",
                        false, "Rollback Migrations");

                CommandLine cmd = cliHandler.parseCliArguments(args);

                if (cmd.hasOption("addFood")) {
                    String foodName = cmd.getOptionValue("addFood");
                    addFood(foodName);
                }

                if (cmd.hasOption("deleteFood")) {
                    String foodName = cmd.getOptionValue("deleteFood");
                    deleteFood(foodName);
                }

                if (cmd.hasOption("showFoods") || cmd.hasOption("viewFoods")) {
                    viewFoods();
                }

                if (cmd.hasOption("addItem") && cmd.hasOption("day") && cmd.hasOption("time")
                        && cmd.hasOption("maxAmount")) {
                    String foodName = cmd.getArgs()[0];
                    String day = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String maxAmount = cmd.getOptionValue("maxAmount");

                    addItem(foodName, day, time, maxAmount);
                }

                if (cmd.hasOption("updateItem") && cmd.hasOption("day") && cmd.hasOption("time")
                        && cmd.hasOption("maxAmount")) {
                    String foodName = cmd.getArgs()[0];
                    String day = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String maxAmount = cmd.getOptionValue("maxAmount");

                    updateItem(foodName, day, time, maxAmount);
                }

                if (cmd.hasOption("removeItem") && cmd.hasOption("day") && cmd.hasOption("time")) {
                    String foodName = cmd.getArgs()[0];
                    String day = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");

                    removeItem(foodName, day, time);
                }

                if (cmd.hasOption("viewMenu") || cmd.hasOption("showMenu")) {
                    if (cmd.hasOption("day")) {
                        String day = cmd.getOptionValue("day");
                        showMenu(day);
                    } else {
                        showMenu();
                    }
                }

                if (cmd.hasOption("migrate")) {
                    if (cmd.hasOption("rollback")) {
                        MigrationHelper.rollbackAll(connection);
                    } else {
                        MigrationHelper.migrateAll(connection);
                    }
                }

            } catch (ParseException e) {
                System.err.println(e.getMessage());
            }
        } else {
            System.err.println("Failed to make connection!");
        }
    }

    public void showMenu() throws SQLException {
        List<Menu> menuItems;
        menuItems = menuDao.getAll();

        Viewer<Menu> viewer = new MenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems);
    }

    public void showMenu(String day) throws SQLException {
        List<Menu> menuItems;
        menuItems = menuDao.getByDay(day);

        MenuViewer viewer = new MenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems, new String[] {day});
    }

    public void removeItem(String foodName, String day, String time) throws SQLException {
        if (!foodDao.getByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                System.err.println("There is no meal at that specified time");
            } else {
                MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
                Food food = foodDao.getByName(foodName).get();

                Optional<MenuItem> potentialMenuItem;
                potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
                if (potentialMenuItem.isPresent()) {
                    menuItemDao.delete(potentialMenuItem.get());
                } else {
                    System.err.println(foodName + " is not in the menu at the specified time");
                }
            }
        }
    }

    public void updateItem(String foodName, String day, String time, String maxAmount) throws SQLException {
        if (!foodDao.getByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.save(mealTime);
            }

            MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
            Food food = foodDao.getByName(foodName).get();

            Optional<MenuItem> potentialMenuItem;
            potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
            if (potentialMenuItem.isPresent()) {
                MenuItem menuItem = potentialMenuItem.get();
                menuItem.setMaximumAmount(maxAmount);
                menuItemDao.update(mealTime, menuItem);
            } else {
                System.err.println("Item not in menu at specified time.");
            }
        }
    }

    public void addItem(String foodName, String day, String time, String maxAmount) throws SQLException {
        if (!foodDao.getByName(foodName).isPresent()) {
            System.out.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.save(mealTime);
            }

            MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
            Food food = foodDao.getByName(foodName).get();

            if (menuItemDao.getMenuItemByData(mealTime.getId(), food.getId()).isPresent()) {
                System.err.println("Item already in menu, not added");
            } else {
                MenuItem menuItem = new MenuItem(mealTime.getId(), food, maxAmount);
                menuItemDao.save(mealTime, menuItem);
            }
        }
    }

    public void viewFoods() throws SQLException {
        List<Food> foods = foodDao.getAll();
        Viewer<Food> viewer = new FoodViewer();
        viewer.view(foods);
    }

    public void deleteFood(String foodName) throws SQLException {
        Optional<Food> potentialFood = foodDao.getByName(foodName);
        if (potentialFood.isPresent()) {
            Food food = potentialFood.get();
            foodDao.delete(food);
        } else {
            System.err.println(foodName + " does not exist in list");
        }
    }

    public void addFood(String foodName) throws SQLException {
        Food food = new Food(foodName);
        foodDao.save(food);
    }
}
