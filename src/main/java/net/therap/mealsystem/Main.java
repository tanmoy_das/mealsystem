package net.therap.mealsystem;

import net.therap.mealsystem.controller.App;
import net.therap.mealsystem.util.MySqlConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/5/20
 */
public class Main {

    public static void main(String[] args) {
        try (Connection connection = MySqlConnectionManager.createConnection()) {
            App app = new App(connection);
            app.run(args);
        } catch (SQLException e) {
            System.err.format("SQL State: %s%n%s", e.getSQLState(), e.getMessage());
        }
    }
}
